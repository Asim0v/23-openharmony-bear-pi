#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "ohos_init.h"
#include "cmsis_os2.h"

#include "lwip/sockets.h"
#include "wifi_connect.h"
#include "wifiiot_gpio.h"
#include "wifiiot_gpio_ex.h"

#define _PROT_ 8888
#define TCP_BACKLOG 10

//在sock_fd 进行监听，在 new_fd 接收新的链接
int sock_fd, new_fd;

char recvbuf[512];
char *buf = "Hello! I'm BearPi-HM_Nano Led TCP Server!";
void sendCommandToClient(int clientSocket, const char *command)
{
    ssize_t ret = send(clientSocket, command, strlen(command) + 1, 0);
    if (ret == -1)
    {
        perror("send : ");
    }
}


static void LedTask(int state)
{
    //初始化GPIO
    GpioInit();

    //设置GPIO_2的复用功能为普通GPIO
    IoSetFunc(WIFI_IOT_IO_NAME_GPIO_2, WIFI_IOT_IO_FUNC_GPIO_2_GPIO);

    //设置GPIO_2为输出模式
    GpioSetDir(WIFI_IOT_GPIO_IDX_2, WIFI_IOT_GPIO_DIR_OUT);

    //根据state参数控制LED灯开关
    if (state == 1)
    {
        //设置GPIO_2输出高电平点亮LED灯
        GpioSetOutputVal(WIFI_IOT_GPIO_IDX_2, 1);
	sendCommandToClient(new_fd, "on");
    }
    else
    {
        //设置GPIO_2输出低电平熄灭LED灯
        GpioSetOutputVal(WIFI_IOT_GPIO_IDX_2, 0);
	sendCommandToClient(new_fd, "off");
    }
}

static void TCPServerTask(void)
{
    //服务端地址信息
    struct sockaddr_in server_sock;

    //客户端地址信息
    struct sockaddr_in client_sock;
    int sin_size;

    struct sockaddr_in *cli_addr;

    //连接Wifi
    WifiConnect("cdy", "15062835505");

    //创建socket
    if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket is error\r\n");
        exit(1);
    }

    bzero(&server_sock, sizeof(server_sock));
    server_sock.sin_family = AF_INET;
    server_sock.sin_addr.s_addr = htonl(INADDR_ANY);
    server_sock.sin_port = htons(_PROT_);

    //调用bind函数绑定socket和地址
    if (bind(sock_fd, (struct sockaddr *)&server_sock, sizeof(struct sockaddr)) == -1)
    {
        perror("bind is error\r\n");
        exit(1);
    }

    //调用listen函数监听(指定port监听)
    if (listen(sock_fd, TCP_BACKLOG) == -1)
    {
        perror("listen is error\r\n");
        exit(1);
    }

    printf("start accept\n");

    //调用accept函数从队列中
    while (1)
    {
        sin_size = sizeof(struct sockaddr_in);

        if ((new_fd = accept(sock_fd, (struct sockaddr *)&client_sock, (socklen_t *)&sin_size)) == -1)
        {
            perror("accept");
            continue;
        }

        cli_addr = malloc(sizeof(struct sockaddr));

        printf("accept addr\r\n");

        if (cli_addr != NULL)
        {
            memcpy(cli_addr, &client_sock, sizeof(struct sockaddr));
        }

        //处理目标
        ssize_t ret;

        while (1)
        {
	    memset(recvbuf, 0, sizeof(recvbuf));
            if ((ret = recv(new_fd, recvbuf, sizeof(recvbuf), 0)) == -1)
            {
                printf("recv error \r\n");
            }
            printf("recv :%s\r\n", recvbuf);
            
            //根据接收到的客户端信息控制LED灯开关
            if (strcmp(recvbuf, "led on") == 0)
            {
                LedTask(1); // 打开LED灯
            }
            else if (strcmp(recvbuf, "led off") == 0)
            {
                LedTask(0); // 关闭LED灯
            }

            //if ((ret = send(new_fd, buf, strlen(buf) + 1, 0)) == -1)
            //{
            //    perror("send : ");
            //}

        }

        close(new_fd);
    }
}

static void TCPServerDemo(void)
{
    osThreadAttr_t attr;

    attr.name = "TCPServerTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size =10240;
    attr.priority = osPriorityNormal;

    if (osThreadNew((osThreadFunc_t)TCPServerTask, NULL, &attr) == NULL)
    {
        printf("[TCPServerDemo] Failed to create TCPServerTask!\n");
    }
}

APP_FEATURE_INIT(TCPServerDemo);
